{% extends htdocs_layout %}

{% block head_title %}Installation{% endblock %}
{% block head_pagedescription %}Installation{% endblock %}

{% block content %}
<p>
  <strong>Intended Audience:</strong>
  System administrators
</p>

<ol>
  <li><a href="#prereq">Prerequisites</a></li>
  <li>
    <a href="#install">Installation (ansible)</a>
    <ol>
      <li><a href="#prep-inv">Prepare ansible inventory</a></li>
      <li><a href="#server">&AElig;-DIR servers</a></li>
      <li><a href="#linux-login">NSS/PAM self-integration</a></li>
      <li><a href="#oath-ldap">Two-factor authentication (OATH-LDAP)</a></li>
    </ol>
  </li>
  <li><a href="#support">Support</a></li>
  <li><a href="#license">Copyright &amp; License</a></li>
  <li><a href="#software">Software / technology used</a></li>
</ol>

<h1 id="prereq">Prerequisites</h1>
<ol>
  <li>
    Make yourself familiar with the
    <a href="docs.html#sys-arch">system architecture</a>.
  </li>
  <li>
    Install &AElig;-DIR servers with one of the supported operating
    systems which support Python 3.6+ out-of-the-box.
    Currently the ansible playbooks support fully automated
    installation/configuration on&hellip;
    <ul>
      <li>
        <a href="https://en.opensuse.org/openSUSE:Tumbleweed_installation">
        openSUSE Tumbleweed</a>
      </li>
      <li>
        <a href="https://software.opensuse.org/distributions/leap">
        openSUSE Leap 15.1 and upcoming 15.2</a>
      </li>
      <li>
        <a href="https://www.suse.com/products/server/">
        SUSE Linux Enterprise Server 15SP1</a> (to be tested)
      </li>
      <li>
        <a href="https://www.debian.org/releases/buster/">Debian buster</a>
        using custom OpenLDAP packages from
        <a href="https://www.ae-dir.com/repo/debian/">
          &AElig;-DIR's Debian APT repository
        </a>
      </li>
      <li>
        <a href="https://www.centos.org/">CentOS 7.7+</a>
        using OpenLDAP packages from
        <a href="https://ltb-project.org/wiki/documentation/openldap-rpm#yum_repository">
          YUM repository of LTB project
        </a>
        (Support for CentOS 8 is currently blocked by <a href="https://bugzilla.redhat.com/show_bug.cgi?id=1757157">bug 1757157 - uwsgi in epel 8</a>)
      </li>
    </ul>
    If you want to tweak the ansible roles to install on another OS make
    sure a recent <a href="https://www.openldap.org">OpenLDAP 2.4.48+</a>
    with overlay <em>slapo-deref</em> is available for your OS platform.
    Older releases are explicitly not recommended!
  </li>
  <li>
    The ansible roles needs the following software packages on all target
    machines to be installed:
    <ul>
      <li>Python 3.6 or newer</li>
      <li>lsb_release command (package <em>lsb-release</em>)</li>
    </ul>
  </li>
  <li>
    Install on your admin workstation (the <em>ansible controller</em>):
    <ul>
      <li>Python 3.6 or newer</li>
      <li>
        <a href="https://docs.ansible.com/ansible/intro_installation.html">ansible 2.9.7+</a>
      </li>
      <li>
        <a href="https://git-scm.com/book/en/v2/Getting-Started-Installing-Git">git</a>
      </li>
      <li>
        <a href="http://www.dnspython.org">dnspython</a>
      </li>
    </ul>
    Simple approach for install ansible in a Python virtual environment:
    <pre class="cli">
python3 -m venv /opt/ansible
/opt/ansible/bin/pip install ansible aedir
    </pre>
  </li>
  <li>
    Create DNS entries for all your &AElig;-DIR servers
    following <a href="bcp.html#ae-dir-hostnames">best practices for hostnames</a>.
    Don't forget to add correct reverse DNS entries (PTR RRs) required
    for correctly creating LDIF data for initial load.
  </li>
  <li>
    Configure time synchronisation (NTP) required for reliable replication.
  </li>
  <li>
    Prepare to have SSH access to all &AElig;-DIR servers as user
    <em>root</em> (via <code>su</code> or <code>sudo</code>)
  </li>
  <li>
    Make yourself familiar with how to use command-line options for
    <a href="https://docs.ansible.com/ansible/become.html#command-line-options">ansible become</a>.
  </li>
  <li>
    Check whether you can access the hosts with
    <a href="https://docs.ansible.com/ansible/setup_module.html">ansible setup</a>:
    <pre class="cli">/opt/ansible/bin/ansible all -i 'hostname.example.com,' -m setup</pre>
    The trailing comma after the FQDN is needed when using a hostname!
  </li>
  <li>
    You have to issue X.509 TLS server certificates with appropriate
    <var>CN</var> and <var>subjectAltName</var> values for all replicas
    with your existing PKI's certificate authority.
    <br>
    The anti-security concept of wild-card certificates is not compatible
    with &AElig;-DIR's security concept! Therefore these cannot be used!
    <br>
    If you don't have a PKI yet you can setup a test certificate authority
    (CA) with shell scripts found in
    <a href="https://gitlab.com/ae-dir/pki-scripts">pki-scripts/</a>.
  </li>
</ol>

<h1 id="install">Installation with ansible</h1>
<p>
  Note that there is no official way to install &AElig;-DIR manually.
</p>

<h2 id="prep-inv">Prepare ansible inventory</h2>
<ol>
  <li>
    Get the ansible playbooks and roles:
    <pre class="cli">
git clone https://gitlab.com/ae-dir/ansible-example-site myenv
cd myenv
/opt/ansible/bin/ansible-galaxy install -r requirements.yml
</pre>
  </li>
  <li>
    Edit ansible inventory file <em>myenv/hosts</em> to match your
    hosts/VMs/containers of your installation environment.
  </li>
  <li>
    Edit the files in directory <em>group_vars/</em> to match your environment.
  </li>
</ol>
<p>See also:</p>
<ul>
  <li>
    Comments in file
    <a href="https://gitlab.com/ae-dir/ansible-ae-dir-server/-/blob/master/defaults/main.yml">
      myenv/roles/ae-dir-server/defaults/main.yml
    </a>
    describing the use of all variables you can override to adapt
    installation to your local environment.
  </li>
  <li>
    Ansible documentation about
    <a href="https://docs.ansible.com/ansible/playbooks_variables.html">
    group and host vars</a> to get a good understanding of how values are
    assigned to variables.
  </li>
</ul>

<h2 id="server">&AElig;-DIR servers</h2>
<ol>
  <li>
    Invoke ansible play in sub-directory <code>ansible/</code>
    (here using command <code>su</code>):
    <pre class="cli">/opt/ansible/bin/ansible-playbook ae-dir-server.yml -i myenv/hosts --become -K --become-method=su -e aedir_init=1 -e aedir_keygen=1</pre>
    <ul>
      <li>
        At first run this will generate TLS server key and signed CSR file
        and stops with a message where to find the CSR files on your local
        ansible controller.
      </li>
      <li>
        After signing the CSRs with your CA place the server certificate file(s) into
        directory <code>ae-dir/ansible/myenv/files/</code>.
      </li>
      <li>
        Invoke <var>ansible-playbook</var> command above again to proceed
        with installation.
      </li>
    </ul>
  </li>
  <li>
    Loading the initial LDAP entries defined in file
    <var>/opt/ae-dir/etc/ae-dir-base.ldif</var>
    is done automatically during installation on the host referenced by
    ansible variable <var>aedir_main_provider_hostname</var>
    (default is first provider host).
  </li>
  <li>
    Check the systems' health by invoking the monitoring
    script on all &AElig;-DIR servers as user <em>root</em>.
    By default it is installed to:
    <pre class="cli">/opt/ae-dir/sbin/slapd_checkmk.sh</pre>
</ol>

<h2 id="linux-login">NSS/PAM self-integration</h2>
<p>
  For PAM/NSS client self-integration invoke this ansible play to install
  and configure <a href="aehostd.html">aehostd</a>
  (here using command <code>su</code>):
</p>
<pre class="cli">
/opt/ansible/bin/ansible-playbook aehostd.yml -i myenv/hosts --become -K --become-method=su -l ae-dir-servers
</pre>

<h2 id="oath-ldap">Two-factor authentication (OATH-LDAP)</h2>
<p>
  You can easily use the built-in two-factor authentication based on
  <a href="https://oath-ldap.stroeder.com/">OATH-LDAP</a>.
</p>
<p>
  This is enabled by setting ansible variable
  <var>oath_ldap_enabled: True</var> and then play the complete
  configuration to the &AElig;-DIR servers. Of course you set this flag
  also before the first run. This installs an additional web app and the
  so-called bind listeners on providers and consumers.
</p>
<p>
  Afterwards you have to generate at least one master key pair for
  protecting the token shared secrets (OTP seeds):
</p>
<pre class="cli">
# oathldap-tool genkey --key-path /opt/ae-dir/etc/oath-master-keys/
Generating RSA-2048 key pair...

wrote /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.priv
wrote /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.pub
</pre>
<p>
  Correct the permissions if needed which would also be done by next ansible play:
</p>
<pre class="cli">
# chmod 640 /opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.*
</pre>
<p>
  Then store the new public key in the OATH parameters entry:
</p>
<pre class="cli">
# ldapmodify &lt;&lt;EOF
dn: cn=oath-policy-hotp-users,cn=ae,ou=ae-dir
changetype: modify
replace: oathEncKey
oathEncKey:&lt; file:/opt/ae-dir/etc/oath-master-keys/oathldap_master_key_201806141818.pub

EOF
</pre>

<h1 id="support">Support</h1>
<ul>
  <li>
    <a href="mailto:michael@stroeder.com?SUBJECT=Subscribe to ae-dir-announce">Subscribe to ae-dir-announce</a>
    and get informed about news.
  </li>
  <li>
    If you need more help please consider
    <a href="https://www.stroeder.com/impressum.html">commercial support</a>.
  </li>
</ul>

<h1 id="license">Copyright &amp; License</h1>
<p>
  &copy; 2015-2020 by <a href="https://stroeder.com/impressum.html">Michael Str&ouml;der</a>
</p>
<pre>
  Licensed under the Apache License, Version 2.0 (the "License"); you may
  not use files and content provided on this web site except in compliance
  with the License. You may obtain a copy of the License at

      <a href="https://www.apache.org/licenses/LICENSE-2.0">https://www.apache.org/licenses/LICENSE-2.0</a>

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
</pre>

<h1 id="software">Software / technology used</h1>
<p>
  &AElig;-DIR serves as a good example for
  <a href="https://en.wikipedia.org/wiki/Standing_on_the_shoulders_of_giants">
  standing on the shoulders of giants</a>:
</p>
<ul>
  <li><a href="https://www.kernel.org/">Linux</a></li>
  <li><a href="https://www.openldap.org">OpenLDAP server and client libs</a></li>
  <li><a href="https://www.python.org">Python</a></li>
  <li><a href="https://git-scm.com/">git</a></li>
  <li><a href="https://www.ansible.com">ansible</a></li>
  <li><a href="https://httpd.apache.org">Apache</a></li>
</ul>
<p>
  Other upstream software components installed:
</p>
<ul>
  <li><a href="https://web2ldap.de">web2ldap</a></li>
  <li><a href="https://pypi.python.org/pypi/ldap0">ldap0</a></li>
  <li><a href="https://www.stroeder.com/slapdcheck.html">slapdcheck</a></li>
  <li><a href="https://www.stroeder.com/slapdsock.html">slapdsock</a></li>
  <li><a href="https://oath-ldap.stroeder.com/">OATH-LDAP</a></li>
</ul>
{% endblock content %}
