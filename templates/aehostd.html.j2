{% extends htdocs_layout %}

{% block head_title %}aehostd -- Custom NSS/PAM demon{% endblock %}
{% block head_pagedescription %}aehostd -- Custom NSS/PAM demon{% endblock %}

{% block content %}

<p>
  <strong>Intended Audience:</strong>
  System architects, developers and system administrators
</p>

<ol>
  <li>
    <a href="#intro">Introduction</a></li>
    <ol>
      <li><a href="#features">Specific features</a></li>
      <li><a href="#maps">NSS maps</a></li>
      <li><a href="#aehost-init">Host password initialization</a></li>
    </ol>
  </li>
  <li>
    <a href="#install">Installation</a>
    <ol>
      <li><a href="#pam-nss-modules">NSS/PAM modules</a></li>
    </ol>
  </li>
  <li>
    <a href="#conf">Configuration</a>
    <ol>
      <li><a href="#conf-general">General process parameters</a></li>
      <li><a href="#conf-socket">Socket parameters</a></li>
      <li><a href="#conf-ldap">LDAP connection parameters</a></li>
      <li><a href="#conf-maps">NSS map parameters</a></li>
      <li><a href="#conf-maps">sudo parameters</a></li>
      <li><a href="#conf-pam">PAM parameters</a></li>
    </ol>
  </li>
</ol>

<h1 id="intro">Introduction</h1>

<p>
  While you can integrate your Linux systems with any NSS/PAM service demon
  it is highly recommended to use
  <a href="https://pypi.org/project/aehostd/">aehostd</a>,
  a custom NSS/PAM service implemented
  in <a href="https://www.python.org">Python 3.6+</a>.
</p>

<p>
  The following diagram illustrates the integration of <em>aehostd</em> into
  Linux login:
</p>

{{ img('aehostd', 'aehostd.svg', '&AElig;-DIR integration of Linux with aehostd') }}

<h2 id="features">Specific features</h2>
<p>
  Additionally to what you expect by such a service this custom client
  is has specific functionality for use with &AElig;-DIR:
</p>
<ul>
  <li>
    Binds to &AElig;-DIR either with
    <ul>
      <li><em>simple bind</em> (short-DN form)</li>
      <li><em>SASL/EXTERNAL</em> bind when using TLS client certs</li>
    </ul>
  </li>
  <li>
    <a href="aehostd.html#aehost-init">Host password initialization</a>
    without admin credentials.
  </li>
  <li>
    Sends <em>LDAP Who Am I?</em> extended operation (see {{ rfc(4352) }})
    to find the real DN of their own
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry
    for determining <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    membership.
  </li>
  <li>
    Without specific client configuration it uses search filters for
    searching user, group and sudoers entries based on the attributes found
    in all relevant
    <a href="docs.html#schema-oc-aeSrvGroup">aeSrvGroup</a>
    entries searched before:
    <ul>
      <li>aeLoginGroups</li>
      <li>aeVisibleGroups</li>
      <li>aeVisibleSudoers</li>
    </ul>
    This gives much better performance when having many POSIX systems
    integrated with &AElig;-DIR.
  </li>
  <li>
    When checking user's password it sends
    <em>Session Tracking</em> extended control along with the simple bind
    request with the IP address of the client system and the user name
    (see <a href="https://tools.ietf.org/html/draft-wahl-ldap-session">draft-wahl-ldap-session</a>).
    This gives more valuable information in &AElig;-DIR slapd logs for auditing.
  </li>
  <li>
    Synchronization of SSH authorized keys to a configurable directory.
  </li>
  <li>
    Synchronization of sudoers content to a configurable file.
  </li>
</ul>

<h2 id="maps">NSS maps</h2>
<p>
  The following NSS maps are provided:
</p>
<ul>
  <li>passwd</li>
  <li>group</li>
  <li>hosts</li>
</ul>

<p>
  Besides normal <em>group</em> map this demon returns some virtual
  groups to the calling application:
  <ul>
    <li>
      Virtual groups for the individual <var>gidNumber</var> set in
      <a href="docs.html#schema-oc-aeUser-attributes">aeUser</a> entries.
    </li>
    <li>
      Virtual role groups based on
      <a href="docs.html#schema-oc-aeSrvGroup-attributes">
      rights groups attributes</a>
      <table>
        <tr>
          <th>Attribute</th>
          <th>Group name</th>
          <th>GID</th>
        </tr>
        <tr>
          <td>aeVisibleGroups</td>
          <td>ae-vgrp-role-all</td>
          <td>9000</td>
        </tr>
        <tr>
          <td>aeLoginGroups</td>
          <td>ae-vgrp-role-log</td>
          <td>9001</td>
        </tr>
        <tr>
          <td>aeLogStoreGroups</td>
          <td>ae-vgrp-role-login</td>
          <td>9002</td>
        </tr>
        <tr>
          <td>aeSetupGroups</td>
          <td>ae-vgrp-role-setup</td>
          <td>9003</td>
        </tr>
      </table>
    </li>
  </ul>
</p>

<p>
  Example use-cases for virtual role groups:
</p>
<ul>
  <li>
    Allow all <a href="docs.html#role-setup-admin">setup admins</a> mapped
    to virtual role POSIX group <var>ae-vgrp-role-setup</var> to use
    command <code>su</code> with own password
    (see <a href="https://linux.die.net/man/8/pam_wheel">pam_wheel(8)</a>).
  </li>
  <li>
    Allow all users with log view right mapped to virtual role POSIX group
    <var>ae-vgrp-role-log</var> to read log files by simply setting normal
    group ownership and group permissions (no need to sync POSIX ACLs).
  </li>
</ul>

<h2 id="aehost-init">Initialization of host password</h2>
<p>
  <em>aehostd</em> has a special feature which is very helpful for
  automated enrollment of hosts. It does not require administrative access
  to the machine before correct initialization and also does not require
  other agents besides <em>aehostd</em> to be installed on the host. Mainly
  it is based on a PAM authentication with host password.
</p>

{{ img('aehost-init', 'aehost-init.svg', 'Initializing host password in aehostd') }}

<p>Prerequisites:</p>
<p>
  Host gets installed with correctly configured short bind-DN
  based on the canonical hostname (FQDN)
  (usually in file <em>/etc/aehostd.conf</em>)
  but without a host password
  (usually in file <em>/var/lib/aehostd/aehostd.pw</em>).
</p>

<p>Process steps:</p>
<ol>
  <li>
    A responsible
    <a href="docs.html#role-setup-admin">setup admin</a> adds a new
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry for the canonical
    hostname (FQDN) and sets a new random password for this entry<br>
    or just sets a new random password for an existing
    <a href="docs.html#schema-oc-aeHost">aeHost</a> entry.
  </li>
  <li>
    <a href="docs.html#role-setup-admin">Setup admin</a> connects via SSH
    to the host authenticating as special service account
    <var>aehost-init</var> with the new host password set before.
  </li>
  <li>
    <em>pam_aedir</em> receives the PAM authentication request from <em>sshd</em>.
  </li>
  <li>
    <em>aehostd</em> receives the PAM authentication request for the system
    user account <var>aehost-init</var>.
  </li>
  <li>
    The host password is validated by sending a simple bind
    request on behalf of the locally configured host bind DN.
  </li>
  <li>
    In case the host password is correct it is written to the
    <em>aehostd</em> password file (located by configuration option
    <var>bindpwfile</var>) in case the password stored therein is
    different.
  </li>
</ol>

<h1 id="install">Installation</h1>

<h2 id="pam-nss-modules">NSS/PAM modules</h2>
<p>
  The NSS and PAM front-end modules of Arthur de Jong's <a
  href="https://arthurdejong.org/nss-pam-ldapd/">nss-pam-ldapd</a> (aka
  nslcd) are used for querying the <em>aehostd</em> service via its Unix
  domain socket.
</p>

<h3>Pre-built binary packages</h3>
<p>
  For some platforms there are ready-to-use binary packages available:
</p>
<ul>
  <li>
    <a href="https://www.ae-dir.com/repo/debian/">
      &AElig;-DIR package repository Debian buster
    </a>
  </li>
  <li>
    <a href="https://build.opensuse.org/package/show/home:stroeder:iam/aehostd-modules">
      openSUSE build service: Package <em>aehostd-modules</em> for openSUSE and SLE
    </a>
  </li>
</ul>

<h3>Compile from source</h3>
<p>
  You can compile these modules with different compile-time parameters
  to prevent naming and package collisions with other standard OS packages.
  In the following example the modules are compiled with module name
  <em>aedir</em> and for using Unix domain socket
  <em>/var/run/aehostd/aehostd.sock</em>:
</p>
<pre class="cli">
./configure \
  --with-module-name=aedir \
  --disable-nslcd --disable-pynslcd --disable-kerberos \
  --libdir=/lib64 \
  --with-pam-seclib-dir=/lib/security \
  --disable-utils \
  --with-nss-maps=passwd,group,hosts \
  --with-nslcd-socket=/var/run/aehostd/aehostd.sock
make
make install
</pre>
<p>
  In <em>/etc/nsswitch.conf</em> you add the following lines:
</p>
<pre class="cli">
passwd: files aedir
group:  files aedir
</pre>

<p>OS-specific source packages:</p>
<ul>
  <li>
    For building own Debian packages you can use files in directory<br>
    <a href="https://gitlab.com/ae-dir/client-examples/tree/master/aehostd-modules/debian">
      client-examples/aehostd-modules/debian/
    </a>
  </li>
  <li>
    For building RedHat, CentOS, Fedora packages you can use file<br>
    <a href="https://gitlab.com/ae-dir/client-examples/blob/master/aehostd-modules/redhat-rpmbuild/SPECS/aehostd-modules.spec">
      client-examples/aehostd-modules/redhat-rpmbuild/SPECS/aehostd-modules.spec
    </a>
  </li>
</ul>

<h2 id="config">ansible</h2>
<p>
  Automated installation/configuration can be done with
  <a href="https://gitlab.com/ae-dir/ansible-aehostd">ansible role <em>aehostd</em></a>.
</p>

<h1 id="conf">Configuration</h1>
<p>
  The following options can be set in the configuration file <em>/etc/aehostd.conf</em>:
</p>

<h2 id="conf-general">General process parameters</h2>
<p>
  The following options specify general process parameters:
</p>
<pre class="cfg">
# the user name or ID aehostd should be run as
uid = aehostd
# the group name or ID aehostd should be run as
gid = aehostd

# Level of log details (Integer), see Python's standard logging module
# Default: 20 (INFO)
#loglevel = 20

# Path name of syslog socket:
# Setting this to a string enforces using syslog, empty string results
# in default syslog socket /dev/log being used.
# Default: None (sends log messages to stderr)
#logsocket =

# Interval (seconds) at which internal monitoring data is written to log.
# Setting this to zero or negative value disables monitor logging completely.
# The default is -1.0 (disabled).
#monitor = 60.0
</pre>

<h2 id="conf-socket">Socket parameters</h2>
<p>
  The following options specify handling of the Unix domain socket on which
  <em>aehostd</em> listens for NSS and PAM requests:
</p>
<pre class="cfg">
# Path name of service socket which must match what PAM and NSS modules expect
# Default: /var/run/aehostd/socket
#socketpath = /var/run/aehostd/socket

# timeout (seconds) of service socket
# Default: 10.0 seconds
#sockettimeout = 10.0

# permissions (octal digits) used for service socket
# Default: 0666 (world-readable and -writeable)
#socketperms = 0666
</pre>

<h2 id="conf-ldap">LDAP connection parameters</h2>
<p>
  Various LDAP connection parameters can be set and tuned:
</p>
<pre class="cfg">
# At least one of uri_list or uri_pool must be specified.
# Both uri_list or uri_pool may be specified.
# List of LDAP servers (LDAP URIs) to try first in exactly this order
# no matter what is configured in uri_pool.
# Default: empty list.
uri_list = ldapi://

# List of LDAP servers (LDAP URIs) to try after all LDAP URIs defined with uri_list failed.
# This list gets rotated based on hosts's canonical FQDN for client-side load-balancing.
# Default: empty list.
uri_pool = ldaps://ae-dir-c1.example.com ldaps://ae-dir-c2.example.com ldaps://ae-dir-c3.example.com

# The bind-DN to use when binding as service to AE-DIR with simple bind.
# Preferably the short bind-DN should be used.
binddn = host=srv1.example.com,ou=ae-dir

# The password file to use for simple bind as identity given in binddn.
# Default is: /var/lib/aehostd/aehostd.pw
#bindpwfile = /var/lib/aehostd/aehostd.pw

# Timeout (seconds) used for all LDAP connections/operations
# Default: 6.0
#timelimit = 6.0

# LDAPObject cache TTL used for short-time LDAP search cache.
# Default: 6.0
#cache_ttl = 6.0

# File containing trusted root CA certificate(s).
# Default: None, which means use system-wide trust store.
#tls_cacertfile = /etc/ssl/certs/cacerts.pem

# File containing client certificate used for SASL/EXTERNAL bind.
#tls_cert = /etc/aehostd.crt

# File containing private key used for SASL/EXTERNAL bind.
#tls_key = /etc/aehostd.key

# Time span (seconds) after which aehostd forcibly reconnects.
# Default: 1800 (30 min.)
#conn_ttl = 1800.0
</pre>

<h2 id="conf-maps">NSS map parameters</h2>
<p>
  Parameters related to NSS maps:
</p>
<pre class="cfg">
# Names of passwd entries to ignore
# Default: All user names found in local file /etc/passwd
#nss_ignore_users =
# IDs of passwd entries to ignore
# Default: All UIDs found in local file /etc/passwd
#nss_ignore_uids =
# Names of group entries to ignore
# Default: All user names found in local file /etc/group
#nss_ignore_groups =
# IDs of group entries to ignore
# Default: All UIDs found in local file /etc/group
#nss_ignore_gids =
# Refresh time (seconds) for NSS passwd and group maps
# Default: 60.0 (1 min)
#refresh_sleep = 60.0

# Minimum numeric UID to handle in passwd requests
# Default: 0
#nss_min_uid = 0
# Minimum numeric GID to handle in group requests
# Default: 0
#nss_min_gid = 0
# Maximum numeric UID to handle in passwd requests
# Default: 65500
#nss_max_uid = 65500
# Maximum numeric GID to handle in group requests
# Default: 65500
#nss_max_gid = 65500

# Refresh time (seconds) for hosts maps. Negative values disables hosts refresh.
# Default: -1.0 (disabled)
#netaddr_refresh = -1.0
# Levels (int) to go up for deriving the hosts map search base.
# Default: 2
#netaddr_level = 2

# Which member attribute in aeGroup entries to use
# Default: memberUid
#memberattr = memberUid

# Name prefix used for virtual groups
# Default:
#vgroup_name_prefix = ae-vgrp-

# Number offset (int) to be used for virtual groups
# Default: 9000
#vgroup_rgid_offset = 9000

# Directory name where to store exported SSH authorized keys
# Default: None (disabled)
#sshkeys_dir = /var/lib/aehostd/sshkeys

# passwd string of virtual user account used to authenticate as own aeHost object
# Default: aehost-init:x:9042:9042:AE-DIR virtual host init account:/tmp:/usr/sbin/nologin
#aehost_vaccount = aehost-init:x:9042:9042:AE-DIR virtual host init account:/tmp:/usr/sbin/nologin

# Template string for deriving GECOS field from e.g. user name
# Default: AE-DIR user {username}
#gecos_tmpl = AE-DIR user {username}

# Template string for deriving home directory path name from e.g. user name
# Default: None (disabled)
#homedir_tmpl =

# Login shell to be used if attribute loginShell is not available
#loginshell_default = /usr/sbin/nologin

# Login shell always used not matter what's in attribute loginShell
# Default: None (disabled)
#loginshell_override =
</pre>

<h2 id="conf-maps">sudo parameters</h2>
<p>
  Parameters related to NSS maps:
</p>
<pre class="cfg">
# Path name of sudoers export file to be picked up by privileged helper
# Default: /var/lib/aehostd/ae-dir-sudoers-export
#sudoers_file = /var/lib/aehostd/ae-dir-sudoers-export

# Directory name where privileged helper stores sudoers export file
# Default: /etc/sudoers.d
#sudoers_includedir = /etc/sudoers.d
# pathname of visudo executable
# Default: /usr/sbin/visudo
#visudo_exec = /usr/sbin/visudo
# pathname of visudo cvtsudoers
# Default: /usr/bin/cvtsudoers
#cvtsudoers_exec = /usr/bin/cvtsudoers
</pre>

<h2 id="conf-pam">PAM parameters</h2>
<p>Parameters used by the PAM handler:</p>
<pre class="cfg">
# Cache TTL (seconds) of PAM authc results
# Default: -1 (disabled)
#pam_authc_cache_ttl =

# LDAP filter template used for checking authorization of a user
# Default: None (disabled)
#pam_authz_search =

# Error message sent to user about password change disabled/denied
# Default: None (no details)
#pam_passmod_deny_msg =
</pre>

{% endblock content %}
